"""
Написать функцию которая делает следующее "При заданном целом числе n и кол-ве m посчитайте n + nn + nnn."
        Пример вызова
        calc_many_numbers(5 , 2) => 5 + 55 (60)
        calc_many_numbers(7 , 4) => 7 + 77 + 777 + 7777 (8638)
"""

def calc_many_numbers(n,m):
    my_str=''
    suma=0
    for i in range(m):
        my_str=my_str+str(n)
        suma+=int(my_str)
    return suma

print(calc_many_numbers(7,4))

"""
Напишите функцию, которая принимает имя файла и выводит его расширение. Если расширение у файла определить невозможно, выбросите исключение.
        Пример вызова
            get_ext('text.txt') => 'txt'
            get_ext('.git') => 'git'
            get_ext('somefile.txt.log') => 'log'
            get_ext('script.py') => 'py'
            get_ext('script.py.') => ValueErorr

"""
def get_ext(file):
    ext: str=''
    for char in file:
        if char == '.':
            if file[file.rfind(char)+1:]=='':
                ext='ValueErorr'
            else:
                ext=file[file.rfind(char)+1:]
    return ext

print(get_ext('ninatxtlog'))


"""
Вы принимаете от пользователя последовательность чисел, разделённых запятой. 
Составьте список и кортеж с этими числами.
"""
def Koma(string):
    count=0
    for char in string:
        if char==',':
            count+=1
    return count

def Digit(string):
    count=0
    for char in string:
        if char.isdigit():
            count+=1
    return count

my_string=input('Enter your sequence->')
print(my_string)

if Digit(my_string)!=len(my_string)-Koma(my_string):
    print('Error')
else:
    my_list = list(map(int, my_string.split(',')))
    print(my_list)
    print(tuple(my_list))

"""
Напишите функцию, которая создаёт комбинацию двух списков таким образом: 
на вход два списка [1, 2, 3] (+) [11, 22, 33] -> [1, 11, 2, 22, 3, 33]
"""



def comb_list(list1,list2):
    list_result=[]
    k=0
    i=0
    if len(list1)<=len(list2):
        for j in range(len(list1)*2):
            if j % 2 == 0:
                list_result.append(list1[k])
                k+=1
            else:
                list_result.append(list2[i])
                i+=1
        return list_result

    else:
        for j in range(len(list2)*2):
            if j % 2 == 0:
                list_result.append(list1[k])
                k+=1
            else:
                list_result.append(list2[i])
                i+=1
    return list_result



print(comb_list([1, 2, 3], [11,22,33]))

"""
Пользователь вводит номер месяца, функция возвращает время года.
"""
def season(month_number):
    Season='None'
    if month_number<=0 or month_number>12:
        return Season
    else:
        if month_number==12 or month_number==1 or month_number==2:
            Season='Winter'
        elif month_number>=3 and month_number<=5:
            Season='Spring'
        elif month_number>=6 and month_number<=8:
            Season='Summer'
        else:
            Season='Autumn'
    return Season

month_number=int(input('Enter month number->'))
print('There is ', season(month_number), 'now')

"""
Задача. Вывести таблицу умножения числа M в диапазоне от числа a до числа b. Числа M, a и b вводит пользователь.
            Например:

                Если M=4, a=2, b=9, то результат будет:

                4x2=8

                4x3=12

                4x4=16

                4x5=20

                4x6=24

                4x7=28

                4x8=32

                4x9=36
"""
def multi_table(M,a,b):
    for i in range(a,b+1):
        print(M, '*', i, '=', M*i)

multi_table(4,2,6)

"""
Переписать функцию . которая у вас уже есть password_validate , 
так чтобы выпадало исключение если пароль невалидный
"""

def is_upper(password):
    count=0
    for i in password:
        if i.isupper():
            count+=1
    return count
def validate(password):
    t=True
    try:
        t = len(password)>=8 and is_upper(password)>=1
    except ValueError:
        t = False
    return t

print(validate('nina'))
print(validate('NinaKasimova'))

"""
Написать функцию которая принримает словарь с числовыми значениями. 
Необходимо их все перемножить и вывести на экран.
"""

def dict_multi(dictionary):
    product=1
    for number in dictionary.values():
            product*=number
    return product

print(dict_multi({1: 2, 2:56, 3:109}))

"""
Создайте словарь, в котором ключами будут числа от 1 до 10, 
а значениями эти же числа, возведенные в куб. Использовать выражение-генератор
"""

dict={}

dict.update({i: i*i*i for i in range(1,10)})
print(dict)

"""
Функция принимает два списка. Необходимо вернуть из функции из них словарь таким образом, 
чтобы элементы первого списка были ключами, а элементы второго — соответственно значениями нашего словаря.
"""
def dictionary_list(list1,list2):
    dict={}
    if len(list1)<=len(list2):
        dict.update({list1[i]: list2[i] for i in range(len(list1))})
    else:
        dict.update({list1[i]: list2[i] for i in range(len(list2))})
    return dict

print(dictionary_list([1,'petya'], ['nina', 5,6,8,6,90,89]))


"""
Написать функцию которая на вход принимает текст и возвращает словарь в котором ключами будет слова из строки . а значениями будет кол-во вхождений слова в строку
            
        Пример вызова
    
            count_words("some text some") => {"some": 2 , "text": 1}
"""


def count(stroka,slovo):
    count=0
    count=stroka.count(slovo)
    return count

def slovar(text):
    dict={}
    splitted_text=text.split()
    dict.update({splitted_text[i]: count(text,splitted_text[i]) for i in range(len(splitted_text))})
    return dict

print(slovar('hello world hello dear'))



