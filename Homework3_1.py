"""
Написать функцию которая на вход принимает список и число <b>x_pow</b> , возвращает словарь , где ключами будут
        индексы элементов а значением x_pow степень тех чисел


            from_list_to_dict([4,5,2,3] , 2)
             {
                0: 16,
                1: 25,
                2: 4,
                3: 9,
            }

"""

from math import pow

def from_list_to_dict(my_list,number):
    dict={}
    dict.update({i: int(pow(my_list[i],number)) for i in range(len(my_list))})
    return dict
print(from_list_to_dict([4,5,2,3],2))

"""

Написать функцию которая поменяет цыфры на буквы по принипцу 0-А 1-B 2-C ... etc и ВОЗВРАЩАЕТ строку

        proccess_str('231') => CDB

"""

def process_str(string1):
    new_list = []
    new_string=''
    for i in range(len(string1)):
        if string1[i] == '1':
            new_list.append('A')
        elif string1[i] == '2':
            new_list.append('B')
        elif string1[i] == '3':
            new_list.append('C')
        elif string1[i]=='4':
            new_list.append('D')
        elif string1[i]=='5':
            new_list.append('E')
        elif string1[i]=='6':
            new_list.append('F')
        elif string1[i]=='7':
            new_list.append('G')
        elif string1[i]=='8':
            new_list.append('H')
        elif string1[i]=='9':
            new_list.append('I')
        elif string1[i]=='10':
            new_list.append('J')
    new_string="".join(new_list)
    return new_string

print(process_str('25675'))


"""
Написать функцию по расчету сколько тратится денег на кипячение воды в чайникe. На вход подается начальная
        температура воды и масса воды. Функция должна ВЕРНУТЬ кол-во гривен.


      
            Q = C*m*(t2-t1), где:
            C - удельная теплоёмкость, т.е. энергия, необходимая для нагрева в-ва на 1 градус. Для воды при нормальном давлении (101.325 кПа) это 4200 джоулей.
            m - масса, 1 литр воды при обычных условиях имеет массу 1 кг.
            t2 - верхняя температура нагрева, для нормального давления температура кипения воды 100 градусов.
            t1 - начальная температура = комнатная температура = в моем случае 25,6 гр.
       

             Q измеряется в джоулях.  1Дж = 2.7778e-7 Кват/час. Необходимо перевести с Джуолей в киловато-часы.

            Стоимость электроенергии можно взять как 1,44грн за 1кват/час

"""
C=0.001167
T2=100
COST1=1.44

def cost(temp,massa):

    Q=C*massa*(T2-temp)
    cost=Q*COST1
    return cost

print('The cost is', cost(25,2), 'UAH')

"""
 Написать функцию которая изменяет порядок слов в строке на обратный
"""



def reverse(my_string):
    new_string=''
    new_list=my_string.split()
    new_list.reverse()
    new_string=" ".join(new_list)
    return new_string
print(reverse('Nina Kasimova Vasilyvna'))

"""
Написать функцию которая на вход принимает список и возвращает True если список возрастает
"""

def vozrastanie(my_list):
    new_list=my_list.copy()
    new_list.sort()
    return new_list==my_list

print(vozrastanie([4,7,2,1]))



