# Task 1
"""
Даны два целых числа. Выведите значение наименьшего из них.
"""

number1 = float(input('Enter first number->'))
number2 = float(input('Enter second number->'))
if number1 < number2:
    print('Minimum is', number1)
elif number2 < number1:
    print('Minimum is', number2)
else:
    print('Numbers are equal')

# Task 2

"""
Заданы две клетки шахматной доски. Если они покрашены в один цвет, то выведите слово YES,
        а если в разные цвета — тоNO. Программа получает на вход четыре числа от 1 до 8 каждое, задающие номер столбца
        и номер строки сначала для первой клетки, потом для второй клетки.
"""


row_cell1 = int(input('Enter row for first cell (from 1 to 8)->'))
column_cell1 = int(input('Enter column for first cell (from 1 to 8)->'))
row_cell2 = int(input('Enter row for second cell (from 1 to 8)->'))
column_cell2 = int(input('Enter column for second cell (from 1 to 8)->'))

if row_cell1 > 8 or row_cell1 < 1 or column_cell1 > 8 or column_cell1 < 1 or row_cell2 > 8 or row_cell2 < 1 or column_cell1 > 8 or column_cell2 < 1:
    print('Wrong parameters of cell')
elif row_cell1 == row_cell2 and column_cell1 == column_cell2:
    print('Same cells')
else:
    if ((row_cell1+column_cell1) % 2 == 0 and (row_cell2 +column_cell2)% 2 == 0) or ((row_cell1+column_cell1) % 2 == 1 and (row_cell2 +column_cell2)% 2 == 1):
        print('YES')
    else:
        print('NO')

# Task3
"""
Если вводится температура в градусах по шкале Цельсия, то она переводится в температуру по шкале Фаренгейта.
        Или наоборот: температура по Фаренгейту переводится в температуру по Цельсию.
"""
temp = float(input('Enter temperature->'))

gradus = input('What measuring (celsius, pharyngitis)?->')

if gradus == 'celsius':
    print('Your temperature in pharyngitis is', (temp * 9 / 5) + 32)
elif gradus == 'pharyngitis':
    print('Your temperature in celsius is', (temp - 32) * 5 / 9)
else:
    print('Wrong measuring')

# Task4

"""
Из списка случайных чисел,определить и вывести на экран нечетные числа
"""

import random

number = 10  # будем работать с 10 числами, не знаю можем ли мы уже оперировать понятием "список"
random_number = random.randint(1, 100)  # генерируем случайное число
while number >= 0:
    if random_number % 2 == 1:
        print(random_number)
    random_number = random.randint(1, 100)
    number -= 1

# Task 5
"""
Вводится целое число, обозначающее код символа по таблице ASCII. Определить, это код английской буквы или
        какой-либо иной символ.
"""

number = int(input('Enter integer number->'))
count = 0  # изменится на 1, если найдем такой код символа в нашем списке

if ord('a')<=number<=ord('z') or ord('A')<=number<=ord('Z'):
    print('English letter ', chr(number))
else:
    print('Some another symbol')

# Task 6
"""
Вводятся два целых числа. Проверить делится ли первое на второе. Вывести на экран сообщение об этом, а также
        остаток (если он есть) и частное (в любом случае).
"""
number1 = int(input('Enetr first number->'))
number2 = int(input('Enter second number->'))

if number2 == 0:
    print('Division is impossible')
elif number1 < number2:
    print('We obtain result that is less than 1')
else:
    print('The first number can be divided into second number')
    print('Result is ', int(number1 / number2))
    print('The rest is ', number1 % number2)

# Task 7
"""
Поочередно вводится 5 цифр, вывести их сумму(успользуя for)
"""
suma = 0

for i in range(5):
    print('Enter', i + 1, 'number->')
    number = int(input())
    suma += number

print('Sum of your numbers is', suma)

# Task 8
"""
Даны два целых числа A и В. Выведите все числа от A до B включительно, в порядке возрастания, если A < B, или в
        порядке убывания в противном случае.
"""
number1 = int(input('Enter first number->'))
number2 = int(input('Enter second number->'))

if number1 == number2:
    print('You have the same number!')
elif number1 < number2:
    for i in range(number1, number2 + 1):
        print(i)
else:
    for i in range(number1, number2 - 1, -1):
        print(i)

# Task 9
"""
Циклом for вывести ромб

                    *
                   * *
                  * * *
                 * * * *
                * * * * *
               * * * * * *
              * * * * * * *
             * * * * * * * *
            * * * * * * * * *
             * * * * * * * *
              * * * * * * *
               * * * * * *
                * * * * *
                 * * * *
                  * * *
                   * *
                    *

"""
size = int(input('Enter size->'))
# верхний треугольник
for i in range(size, 0, -1):
    for j in range(i):
        print(' ', end='')
    for m in range(size - i):
        print('* ', end='')
    for k in range(i):
        print(' ', end='')
    print()

# нижний треугольник
for i in range(size):
    for j in range(i):
        print(' ', end='')
    for m in range(size - i):
        print('* ', end='')
    for k in range(i):
        print(' ', end='')
    print()

# Task 10
"""
Посчитать сумму числового ряда от 0 до 14 включительно. Например, 0+1+2+3+…+14;
"""
suma = 0
for i in range(1, 15):
    print(suma, '+', i, '=', suma + i)
    suma += i

print('Suma=', suma)

# Task 11
"""
Перемножить все чётные значения в диапазоне от 0 до 436
"""
product = 1

for i in range(1, 437):
    if i % 2 == 0:
        product *= i
print(product)

# Task 12
"""
Задается случайное вещественное число. Определить максимальную цифру этого числа.
        Пример выполнения:
        56.457 ->
        7
"""
a = float(input('Enter number->'))
a = abs(a)  # для работы с положительным числом
while a - int(a) != 0.0:  # делаем число целым
    a *= 10
# print(a)
max = abs(a % 10)  # пусть первая отброшенная цифра будет макимальной

while a > 1:  # проводим манипуляции с числом, отбрасываем цифру, и потом рассматриваем целое число без нее
    a = a / 10
    if abs(a % 10) >= max:
        max = abs(a % 10)
print(int(max))

# Task 13

"""
Факториалом числа n называется число 𝑛!=1∙2∙3∙…∙𝑛. Создайте программу, которая вычисляет факториал введённого
        пользователем числа.(цыкл!)
"""
number = int(input('Enter number->'))
factorial = 1
if number <= 0:
    print('Wrong number!')
else:
    for i in range(1, number + 1):
        factorial *= i
print('Factorial of ', number, '=', factorial)

# Task14
"""
Используя вложенные циклы и функции print(‘*’, end=’’), print(‘ ‘, end=’’) и print() выведите на экран
        прямоугольный треугольник.
"""
size = int(input('Enter size of triangle->'))

for i in range(size):
    for j in range(size):
        if i > j:
            print('*  ', end='')
        else:
            print(' ', end='')
    print()

print()

# Task 15
"""

Пользователь делает вклад в размере N $ сроком на years лет под 11.5% годовых (каждый год размер его вклада
        увеличивается на 11.5%. Эти деньги прибавляются к сумме вклада, и на них в следующем году тоже будут проценты).
        Написать программу , где пользователь вводит аргументы a и years, и посчитать сумму, которая будет на счету
        пользователя через years лет.
"""
vklad = float(input('Enter your deposit->'))
procent = 11.5
years = int(input('Enter period->'))
from math import pow

print('Your amount of money after ', years, 'years is', round(vklad * pow((1 + 11.5 / 100), years), 3), 'dollars')

# Task 16
"""
Пользователь вводит год. Определить он высокосный или нет.
"""
year = int(input('Enter year->'))

if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print('Leap year!')
        else:
            print('Is not a leap year')
    else:
        print('Leap year!')

else:
    print('Is not a leap year')

# Task 17
"""
 Напишите программу, которая запрашивает три целых числа a, b и x и выводит True, если x лежит между a и b, иначе
        – False.
"""
number1 = int(input('Enter first number->'))
number2 = int(input('Enter second number->'))
number3 = int(input('Enter third number->'))

if number1 <= number3 <= number2:
    print('True')
else:
    print('False')

# Task 18
"""
Даны четыре действительных числа: x1, y1, x2, y2. вычислите расстояние между точкой (x1,y1) и (x2,y2). Считайте
        четыре действительных числа и выведите результат работы этой функции.
"""
x1 = int(input('Enter x1->'))
x2 = int(input('Enter x2->'))
y1 = int(input('Enter y1->'))
y2 = int(input('Enter y2->'))
from math import sqrt, pow

dist = sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2))
print('Distance between (x1,y1) and (x2,y2) = ', dist)




