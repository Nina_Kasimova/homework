from datetime import datetime
import pandas
import json

print()

#створюємо типу базу зареєстрованих користувачів у вигляді файлу

acount=[{'number': '+380876545457', 'password': 'qweqwe!adSD'},{'number': '+380965674543', 'password': 'qwertyQWERTY!'}, {'number': '+380987897867', 'password': 'qwerty1QWERTY1!'}]

with open('acount', 'w') as file:
    json.dump(acount, file)
with open('acount', 'r') as file:
    user_acount = json.load(file)

class Product:
    def __init__(self, name_pr, price, art, is_sale=False, discount=0):
        self.name_pr=name_pr
        self.price = price
        self.art = art
        self.is_sale = is_sale
        self.discount = discount

    def __str__(self):
        return """
    Product
        - name_pr = {name_pr}
        - price = {price}$
        - art = {art}
        - is_sale  = {is_sale}
        - discount = {discount}    
    """.format(name_pr=self.name_pr, price=self.price, art=self.art, is_sale='YES' if self.is_sale else 'NO', discount=self.discount)

    def __repr__(self):
        return 'Product(name_pr={}, price={}, art={}, is_sale={}, discount={})'.format(self.name_pr, self.price, self.art, 'YES' if self.is_sale else 'NO', self.discount)

    def get_price(self):
        if not self.is_sale:
            return self.price
        return (1 - self.discount / 100) * self.price

    def __eq__(self, other):
        return self.name_pr==other.name_pr and self.price==other.price and self.art==other.art and self.is_sale==other.is_sale and self.discount==other.discount




class Basket:
    def __init__(self, date=datetime.now().strftime("%d-%m-%Y  %H:%M")):
        self.products=[]
        self.date=date

    def show(self):
        print(self.products)


    def add_product(self, product):
        self.products.append(product)
        self.date=datetime.now().strftime("%d-%m-%Y  %H:%M")

    def del_product(self,product):
        for elem in self.products:
            if elem==product:
                self.products.remove(product)
        self.date = datetime.now().strftime("%d-%m-%Y  %H:%M")

    def total_price(self):
        suma=0
        for elem in self.products:
            suma+=elem.get_price()
        return suma

    def total_price_without_sale(self):
        suma=0
        for elem in self.products:
            suma+=elem.price
        return suma

    def count(self):
        count=0
        for elem in self.products:
            if elem.is_sale==True:
                count+=1
        return count

    def economy_percent(self):
        if self.count()!=0:
            return round(100-((self.total_price()*100)/self.total_price_without_sale()),2)
        else:
            return 0

    def get_date(self):
        return self.date

    def products1(self):
        list1=[]
        for i in range(len(self.products)):
            list1.append({'name': self.products[i].name_pr, 'price': self.products[i].price, 'art':self.products[i].art, 'is_sale': self.products[i].is_sale, 'discount': self.products[i].discount })
        return list1

    def file(self):
        with open('basket', 'w') as file:
            json.dump(self.products1(), file)
        with open('basket', 'r') as file:
            basket = json.load(file)
        pandas.read_json("basket").to_excel("basket.xlsx")



class User:
    def __init__(self, name, surname, password, phone_number):
        self.validate_user_data(password=password, phone_number=phone_number)
        self.phone_number=phone_number
        self.__password=password
        self.name = name
        self.surname=surname
        self.basket=Basket(datetime.now().strftime("%d-%m-%Y  %H:%M"))




    def __str__(self):
        return "{} {} {}".format(self.name, self.surname, self.phone_number)

    def validate_password(self, password):
        count_upper_chars = 0
        count_spec_chars = 0
        spec_chars = '.,!?;'
        for elem in password:
             if ord(elem) in range(ord('A'), ord('Z') + 1):
                 count_upper_chars += 1
             if elem in spec_chars:
                 count_spec_chars += 1
        if count_upper_chars < 2 or count_spec_chars < 1:
            raise ValueError(' "{}"  invalid password!'.format(password))



    def validate_number(self, phone_number):
        if not (phone_number[1:].isdigit() and len(phone_number)==13 and phone_number[0:4]=='+380'):
            raise ValueError('{} invalid phone number'.format(phone_number))

    def validate_user_data(self, password, phone_number):
        self.validate_password(password)
        self.validate_number(phone_number)

    def change_password(self, phone_number, new_password):
         self.validate_user_data(new_password, phone_number)
         if phone_number == self.phone_number:
             self.__password = new_password
             for elem in user_acount:
                if elem['number']==self.phone_number:
                    elem['password']=self.__password
             with open('acount', 'w') as file:
                json.dump(user_acount, file)
             return True
         else:
            raise ValueError('number is incorrect')



    def enter_acount(self, phone_number, password):
        if {'number': phone_number, 'password': password} in user_acount:
            print('You are in acount')
        else:
            print('You are not registered user')
            exit()
        """for elem in user_acount:
            if elem['number']==phone_number and elem['password']==password:
                print('You are in acount')
                print(user_acount)
            else:
                print('You are not registered user')
                exit()"""

    def exit_acount(self):
        print('Bye!')
        exit()

    def write_to_json(self):
        data=[{'name': self.name, 'surname': self.surname, 'number': self.phone_number} ]
        with open('data', 'w') as file:
            json.dump(data, file)

    def show_basket(self):
        self.basket.show()




pr=Product(name_pr='Apple', price=200, art='45567', is_sale=True, discount=25)
pr1=Product('Peach', 150, '6785', True, 20)

user1 = User(name="Nina", surname="Kasimova",
              phone_number="+380876545457", password='qweqwe!adSD')

print(user1)
print()
print('Do You want to enter to Your acount? Enter the phone number and password')
number=input('number-->')
password=input('password-->')
user1.enter_acount(number,password)
print()
print('You have not products in Your basket yet')
user1.show_basket()
print('You can add products (user, for example add Apple, Peach and Watermelon)')
user1.basket.add_product(pr)
user1.basket.add_product(pr1)
user1.basket.add_product(Product('Watermelon', 120, '5678', True, 0))
print()
print('Your basket now is:')
user1.show_basket()
print('Last changes in basket is ', user1.basket.date)
print()
print('You can del some product (user, for example, delete Watermelon)')
user1.basket.del_product(Product('Watermelon', 120, '5678', True, 0))


print()
print('Your basket now is:')
user1.show_basket()
print('Last changes in basket is ', user1.basket.date)
print()
print('You can see Your basket at xls-file (the xls-file is created)')
user1.basket.file()
print()
print('Your total price is:')
print(user1.basket.total_price())
print()
print('The economy is ')
print(user1.basket.economy_percent(), ' percents')
print()
print('You can change Your password (note, that new password is rewritten to file acount)')
number1=input('phone number-->')
password1=input('password-->')
user1.change_password(number1, password1)
print('Your password is changed in file acount')
print()
print('You can see Your data in json-file (json-file is created)')
user1.write_to_json()
print()

print('Now You can exit Your acount')
user1.exit_acount()




