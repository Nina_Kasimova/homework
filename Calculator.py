import json
import math
from datetime import datetime


def add(a, b): return a + b

def sub(a, b): return a - b

def div(a, b): return a / b

def mul(a, b): return a * b

def sinus(angle): return math.sin(angle)

def cosinus(angle): return math.cos(angle)

def tangens(angle): return math.tan(angle)

def cotangens(angle): return 1/(math.tan(angle))



calc_obj = {
     '+': add,
     '-': sub,
     '/': div,
     '*': mul,
 }

calc_obj1={
     'sin': sinus,
     'cos': cosinus,
     'tg': tangens,
     'ctg': cotangens,
    }


#сразу создаю файл как историю юзера, куда буду дописывать операцию выбранную, чтоб при запуске кода уже файл создался
user_history=[{'Id' : 1, 'date': '20-10-1953', 'operation': '2+2=4'},{'Id': 2, 'date': '21-12-2014', 'operation': '12/3=4'}, {'Id':3,'date': '31-03-2022', 'operation': 'sin(0)=0'}]
with open('user_history', 'w') as file:
    json.dump(user_history, file)
with open('user_history', 'r') as file:
    history = json.load(file)


def calc1():
    try:
        first_num = float(input('Enter first number >>>> '))
        operation = input('Choose the operation {} >>>'.format(" ".join(calc_obj.keys())))
        second_num = float(input('Enter the second number >>>> '))
        result = calc_obj[operation](first_num, second_num)
        print(result)
        #строка результат, которая будет записываться в файл-историю
        str1=str(first_num)+str(operation)+str(second_num)+'='+str(result)
        with open('user_history', 'r') as file:
            history = json.load(file)
        user_history=history
        user_history.append({'Id': len(user_history)+1, 'date': datetime.now().strftime("%d-%m-%Y"), 'operation':str1})
        with open('user_history', 'w') as file:
            json.dump(user_history, file)
    except ValueError as err:
        print('Wrong value!')
    except KeyError as e:
        print('invalid operation!')
    except ZeroDivisionError as e:
        print('You cannot divide into zero!')
    except Exception as e:
        print(e, type(e))



def calc2():
    try:
        angle = float(input('Enter the angle in radians >>>> '))
        operation = input('Choose the operation {} >>>'.format(" ".join(calc_obj1.keys())))
        result = calc_obj1[operation](angle)
        print(result)
        str2=str(operation)+'('+str(angle)+')'+'='+str(result)
        with open('user_history', 'r') as file:
            history = json.load(file)
        user_history=history
        user_history.append({'Id': len(user_history)+1, 'date': datetime.now().strftime("%d-%m-%Y"), 'operation':str2})
        with open('user_history', 'w') as file:
            json.dump(user_history, file)

    except ValueError as err:
        print('Wrong value!')
    except KeyError as e:
        print('invalid operation!')
    except ZeroDivisionError as e:
        print('You cannot divide into zero!')
    except Exception as e:
        print(e, type(e))

def auth(login, password,user_list):
    user_dict={}
    user_dict['login']=login
    user_dict['password']=password
    if user_dict in user_list:
        return
    raise ValueError

def povtor(login, list1):
    k=0
    for elem in list1:
        if elem.get('login')==login:
           k+=1
    return k

def history(history_list):
    print('Your full history:')
    from pprint import pprint
    pprint(history_list)
    filtered_history=[]
    for elem in history_list:
        if elem['date']==datetime.now().strftime("%d-%m-%Y"):
            filtered_history.append(elem)
    print('Filtered history by today:')
    pprint(filtered_history)

print('''Calculator:
      1. Registration
      2. Authorization
      ''')


data_dict={}
# типа сразу имеем базу зарегистрированных юзеров, поскольку при запуске кода сразу будет создан файл
data=[{'login': 'ADMIN', 'password': 'QWERTY'}]

with open('data', 'w') as file:
    json.dump(data, file)
with open('data', 'r') as file:
    read_data1 = json.load(file)



answer1 = int(input('Choose Your action: '))

if answer1 == 1:
    print('Your choice is Registration')
    login1=input('Enter probable login>>>>')
    password1=input('Enter probable password>>>>')
    data_dict['login']=login1
    data_dict['password']=password1
    with open('data', 'r') as file:
        read_data1 = json.load(file)
    data=read_data1
    #чтоб не повторялись юзеры с одинаковыми логинами
    if povtor(login1,data)!=0:
        print('There is user with such login')
    #иначе записываем в базу этого юзера
    else:
        data.append(data_dict)
        with open('data', 'w') as file:
            json.dump(data, file)
        with open('data', 'r') as file:
            read_data1 = json.load(file)
        answer2 = int(input('''Continue with authorization?: 
                        1. Yes
                        2. No
                       '''))
        if answer2==1:
            login2 = input('Enter login>>>')
            password2 = input('Enter password>>>')
            with open('data', 'r') as file:
                read_data3 = json.load(file)
            #проверяем есть ли такой юзер среди зарегистрированных
            auth(login2, password2, read_data3)
            answer3=int(input('''Arithmetic or trigonometric operation
                        1. Arithmetic
                        2. Trigonometric
                '''))
            if answer3==1:
                calc1()
                #сразу предлагаем просмотреть историю всю или по выбранной дате (в качестве примера - текущая)
                with open('user_history', 'r') as file:
                    history11 = json.load(file)
                history(history11)
            elif answer3==2:
                calc2()
                with open('user_history', 'r') as file:
                    history11 = json.load(file)
                history(history11)
            else:
                print('Wrong choice')
        elif answer2==2:
            calc1()
            with open('user_history', 'r') as file:
                history11 = json.load(file)
            history(history11)
        else:
            print('Wrong choice')
elif answer1==2:
    login3 = input('Enter login>>>')
    password3 = input('Enter password>>>')
    with open('data', 'r') as file:
        read_data4 = json.load(file)
    auth(login3, password3, read_data4)
    answer4 = int(input('''Arithmetic or trigonometric operation
                            1. Arithmetic
                            2. Trigonometric
                    '''))
    if answer4 == 1:
        calc1()
        with open('user_history', 'r') as file:
            history11 = json.load(file)
        history(history11)
    elif answer4 == 2:
        calc2()
        with open('user_history', 'r') as file:
            history11 = json.load(file)
        history(history11)
    else:
        print('Wrong choice')
else:
    print('Wrong choice')

















