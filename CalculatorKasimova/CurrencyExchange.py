import requests

def currency():
    response_list  = requests.get(" https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5").json()


    print('''Currency exchange:
        1. USD<->UAH
        2. EUR<->UAH
        3. BTC<->USD
        ''')

    choice = int(input('Choose currency: '))

    if choice == 1:
        print('Your choice is USD<->UAH')
        print('''Available operations:
            1. Buy USD
            2. Sale USD
        ''')
        choice1=int(input('Choose desirable operation:'))
        if choice1==1:
            amount=float(input('How much USD you want to buy?'))
            print('You have to pay ', amount*float(response_list[0]["sale"]), 'of UAH')
        elif choice1==2:
            amount = float(input('How much USD you want to sale?'))
            print('You receive ', amount * float(response_list[0]["buy"]), 'of UAH')
        else:
            print('Wrong choice')

    elif choice == 2:
        print('Your choice is EUR<->UAH')
        print('''Available operations:
                1. Buy EUR
                2. Sale EUR
            ''')
        choice1 = int(input('Choose desirable operation:'))
        if choice1 == 1:
            amount = float(input('How much EUR you want to buy?'))
            print('You have to pay ', amount * float(response_list[1]["sale"]), 'of UAH')
        elif choice1 == 2:
            amount = float(input('How much EUR you want to sale?'))
            print('You receive ', amount * float(response_list[1]["buy"]), 'of UAH')
        else:
            print('Wrong choice')

    elif choice == 3:
        print('Your choice is BTC<->USD')
        print('''Available operations:
                1. Buy BTC
                2. Sale BTC
            ''')
        choice1 = int(input('Choose desirable operation:'))
        if choice1 == 1:
            amount = float(input('How much BTC you want to buy?'))
            print('You have to pay ', amount * float(response_list[2]["sale"]), 'of USD')
        elif choice1 == 2:
            amount = float(input('How much BTC you want to sale?'))
            print('You receive ', amount * float(response_list[2]["buy"]), 'of USD')
        else:
            print('Wrong choice')
    else:
        print('Wrong choice')
