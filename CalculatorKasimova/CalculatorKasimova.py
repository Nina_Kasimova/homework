import json
import math
from datetime import datetime
import CurrencyExchange


def add(a, b): return a + b

def sub(a, b): return a - b

def div(a, b): return a / b

def mul(a, b): return a * b

def sinus(angle): return math.sin(angle)

def cosinus(angle): return math.cos(angle)

def tangens(angle): return math.tan(angle)

def cotangens(angle): return 1/(math.tan(angle))



calc_obj = {
     '+': add,
     '-': sub,
     '/': div,
     '*': mul,
 }

calc_obj1={
     'sin': sinus,
     'cos': cosinus,
     'tg': tangens,
     'ctg': cotangens,
    }


#сразу создаю файл как историю юзера, куда буду дописывать операцию выбранную, чтоб при запуске кода уже файл создался
user_history=[{'ADMIN': {'Id' : 1, 'date': '20-10-1953', 'operation': '2+2=4'}},{'ADMIN': {'Id': 2, 'date': '21-12-2014', 'operation': '12/3=4'}}, {'ADMIN':{'Id':3,'date': '31-03-2022', 'operation': 'sin(0)=0'}}]
with open('user_history', 'w') as file:
    json.dump(user_history, file)
with open('user_history', 'r') as file:
    history = json.load(file)


def calc11():
    try:
        first_num = float(input('Enter first number >>>> '))
        operation = input('Choose the operation {} >>>'.format(" ".join(calc_obj.keys())))
        second_num = float(input('Enter the second number >>>> '))
        result = calc_obj[operation](first_num, second_num)
        print(result)
        #строка результат, которая будет записываться в файл-историю
        str1=str(first_num)+str(operation)+str(second_num)+'='+str(result)
        with open('user_history', 'r') as file:
            history = json.load(file)
        user_history=history
        user_history.append({'Id': len(user_history)+1, 'date': datetime.now().strftime("%d-%m-%Y"), 'operation':str1})
        with open('user_history', 'w') as file:
            json.dump(user_history, file)
    except ValueError as err:
        print('Wrong value!')
    except KeyError as e:
        print('invalid operation!')
    except ZeroDivisionError as e:
        print('You cannot divide into zero!')
    except Exception as e:
        print(e, type(e))

def calc1(string1):
    try:
        first_num = float(input('Enter first number >>>> '))
        operation = input('Choose the operation {} >>>'.format(" ".join(calc_obj.keys())))
        second_num = float(input('Enter the second number >>>> '))
        result = calc_obj[operation](first_num, second_num)
        print(result)
        #строка результат, которая будет записываться в файл-историю
        str1=str(first_num)+str(operation)+str(second_num)+'='+str(result)
        with open('user_history', 'r') as file:
            history = json.load(file)
        user_history=history
        user_history.append({string1:{'Id': len(user_history)+1, 'date': datetime.now().strftime("%d-%m-%Y"), 'operation':str1}})
        with open('user_history', 'w') as file:
            json.dump(user_history, file)
    except ValueError as err:
        print('Wrong value!')
    except KeyError as e:
        print('invalid operation!')
    except ZeroDivisionError as e:
        print('You cannot divide into zero!')
    except Exception as e:
        print(e, type(e))

def calc2(string1):
    try:
        angle = float(input('Enter the angle in radians >>>> '))
        operation = input('Choose the operation {} >>>'.format(" ".join(calc_obj1.keys())))
        result = calc_obj1[operation](angle)
        print(result)
        str2=str(operation)+'('+str(angle)+')'+'='+str(result)
        with open('user_history', 'r') as file:
            history = json.load(file)
        user_history=history
        user_history.append({string1:{'Id': len(user_history)+1, 'date': datetime.now().strftime("%d-%m-%Y"), 'operation':str2}})
        with open('user_history', 'w') as file:
            json.dump(user_history, file)

    except ValueError as err:
        print('Wrong value!')
    except KeyError as e:
        print('invalid operation!')
    except ZeroDivisionError as e:
        print('You cannot divide into zero!')
    except Exception as e:
        print(e, type(e))

def CurEx(string1):
    CurrencyExchange.currency()
    str3 = 'CurrencyExchange'
    with open('user_history', 'r') as file:
        history = json.load(file)
    user_history = history
    user_history.append(
        {string1:{'Id': len(user_history) + 1, 'date': datetime.now().strftime("%d-%m-%Y"), 'operation': str3}})
    with open('user_history', 'w') as file:
        json.dump(user_history, file)

def auth(login, password,user_list):
    user_dict={}
    user_dict['login']=login
    user_dict['password']=password
    if user_dict in user_list:
        return 1
    else:
        return 0
def validation(password):
    count = 0
    for elem in password:
        if 'A' <= elem <= 'Z':
            count += 1
    if len(password) > 8 and count >= 2:
        return 1
    else:
        return 0

def povtor(login, list1):
    k=0
    for elem in list1:
        if elem.get('login')==login:
           k+=1
    return k

def history(history_list, string1):
    print('Your full history:')
    from pprint import pprint
    full_history=[]
    for elem in history_list:
        if string1 in elem.keys():
            full_history.append(elem)
    pprint(full_history)
    filtered_history=[]
    for elem in history_list:
        for elem1 in elem:
            if elem[elem1]['date']==datetime.now().strftime("%d-%m-%Y"):
                filtered_history.append(elem)
    print('Filtered history by today:')
    pprint(filtered_history)

print('''Calculator:
      1. Registration
      2. Authorization
      
      ''')


data_dict={}
# типа сразу имеем базу зарегистрированных юзеров, поскольку при запуске кода сразу будет создан файл
data=[{'login': 'ADMIN', 'password': 'QWERTY12345'}]

with open('data', 'w') as file:
    json.dump(data, file)
with open('data', 'r') as file:
    read_data1 = json.load(file)



answer1 = int(input('Choose Your action: '))

if answer1 == 1:
    print('Your choice is Registration')
    login1=input('Enter probable login>>>>')
    password1=input('Enter probable password>>>>')
    if validation(password1):

        data_dict['login']=login1
        data_dict['password']=password1
        with open('data', 'r') as file:
            read_data1 = json.load(file)
        data=read_data1
    #чтоб не повторялись юзеры с одинаковыми логинами
        if povtor(login1,data)!=0:
            print('There is user with such login')
    #иначе записываем в базу этого юзера
        else:
            data.append(data_dict)
            with open('data', 'w') as file:
                json.dump(data, file)
            with open('data', 'r') as file:
                read_data1 = json.load(file)
            answer2 = int(input('''Continue with authorization?: 
                            1. Yes
                            2. No
                        '''))
            if answer2==1:
            #login2 = input('Enter login>>>')
            #password2 = input('Enter password>>>')
            #with open('data', 'r') as file:
                #read_data3 = json.load(file)
            #проверяем есть ли такой юзер среди зарегистрированных
                print('You have only three attempts')
                n = 3
                while n > 0:
                    n-=1
                    login2 = input('Enter login>>>')
                    password2 = input('Enter password>>>')
                    with open('data', 'r') as file:
                        read_data3 = json.load(file)
                    if auth(login2, password2, read_data3):
                        answer3 = int(input('''Choose Your action
                                            1. Arithmetic
                                            2. Trigonometric
                                            3. Currency Exchange
                                            4. Exit
                                    '''))
                        if answer3 == 1:
                            calc1(login2)
                        # сразу предлагаем просмотреть историю всю или по выбранной дате (в качестве примера - текущая)
                            his=input('Do You want to see the history?-->')
                            if his=='Y':
                                with open('user_history', 'r') as file:
                                    history11 = json.load(file)
                                history(history11,login2)
                                break
                            else:
                                print('See You!')
                                break
                        elif answer3 == 2:
                            calc2(login2)
                            his = input('Do You want to see the history?-->')
                            if his == 'Y':
                                with open('user_history', 'r') as file:
                                    history11 = json.load(file)
                                history(history11,login2)
                                break
                            else:
                                print('See You!')
                                break
                        elif answer3 == 3:
                            CurEx(login2)
                            his = input('Do You want to see the history?-->')
                            if his == 'Y':
                                with open('user_history', 'r') as file:
                                    history11 = json.load(file)
                                history(history11,login2)
                                break

                        elif answer3 == 4:
                            print('See You!')
                            break
                        else:
                            print('Wrong choice')
                            break
                    else:
                        print('Such user can not be found thus You have only arithmetic calculator')
                        calc11()
                        break
                else:
                    print('Wrong choice')







            elif answer2==2:
                print('You can use only arithmetic calculator')
                calc1(login1)


            else:
                print('Wrong choice')
    else:
        print('Password is not valid')

elif answer1==2:
    #login3 = input('Enter login>>>')
    #password3 = input('Enter password>>>')
    #with open('data', 'r') as file:
        #read_data4 = json.load(file)
    print('You have only tree attempts')
    n=3
    while n>0:
        n-=1
        login3 = input('Enter login>>>')
        password3 = input('Enter password>>>')

        with open('data', 'r') as file:
            read_data4 = json.load(file)
        if auth(login3, password3, read_data4):
            answer4 = int(input('''Choose Your action:
                                       1. Arithmetic
                                       2. Trigonometric
                                       3. Currency Exchange
                                       4. View history
                                       5. Exit
                               '''))
            if answer4 == 1:
                calc1(login3)
                his = input('Do You want to see the history?-->')
                if his == 'Y':
                    with open('user_history', 'r') as file:
                        history11 = json.load(file)
                    history(history11,login3)
                else:
                    print('See You!')
            elif answer4 == 2:
                calc2(login3)
                his = input('Do You want to see the history?-->')
                if his == 'Y':
                    with open('user_history', 'r') as file:
                        history11 = json.load(file)
                    history(history11,login3)
                else:
                    print('See You!')
            elif answer4 == 3:
                CurEx(login3)
                his = input('Do You want to see the history?-->')
                if his == 'Y':
                    with open('user_history', 'r') as file:
                        history11 = json.load(file)
                    history(history11,login3)
                else:
                    print('See You!')
            elif answer4==4:
                k=0
                with open('user_history', 'r') as file:
                    history11 = json.load(file)
                for elem in history11:
                    if login3 in elem.keys():
                        k+=1
                if k:
                    history(history11,login3)
                else:
                        print('You have not history yet')
            elif answer4==5:
                print('See You!')

            else:
                print('Wrong choice')
            break

    else:
        print('Such user can not be found thus You have only arithmetic calculator')
        calc11()

else:
    print('Wrong choice')

















