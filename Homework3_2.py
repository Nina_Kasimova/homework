"""

Написать функцию которая превращает строку в следующйи формат. функция должна ВОЗВРАЩАТЬ строку

            rebuild_string('test') = > tEsT
            rebuild_string('some string') = > sOmE sTrIng
"""


def slova(my_string):
    new_list=[]
    new_list1=[]
    new_list=my_string.split()
    for elem in new_list:
        slovo=''
        for i in range(len(elem)):
            slovo+=elem[i].upper() if (i+1)%2==0 else elem[i]
        new_list1.append(slovo)
    new_string=' '.join(new_list1)
    return new_string

print(slova('mama, mila ramu'))

"""

 Вычисление наибольших общих делителей
"""


def nsd(a,b):
    if a==b:
        result=a
    elif a>b:
        result=nsd(a-b,b)
    else:
        result=nsd(a,b-a)
    return result

def nsd1(*numbers):
    result=numbers[0]
    for i in range(len(numbers)):
        result=nsd(result,numbers[i])
    return result



print(nsd1(6,9,18,21))

"""
 написать фун-цию которая на вход принимает N кол-во списков и возвращает тот список , у которого сумма элементов
        наибольшая

"""


def suma(my_list, i, n):
    if i==n-1:
        return my_list[i]
    else:
        return my_list[i]+suma(my_list,i+1,n)

print(suma([1,2,4,-3],0,4))

def max_list(list1):
    max=list1[0]
    k=0
    for i in range(len(list1)):
        if list1[i]>=max:
            max=list1[i]
            k=i
    return k

print(max_list([1,2,-9,16]))

def max_suma(*lists):
    list_m=[]
    for j in range(len(lists)):
        list_m.append(suma(lists[j], 0, len(lists[j])))
    return lists[max_list(list_m)]

print(max_suma([1,2,3], [-9,6,5,4,3], [0,6,5,-6,5],[6,-6,0]))

"""
Дан список чисел, который может содержать до 100000 чисел. Определите, сколько в нем встречается различных
        чисел. сделать функцию

        most_common(1,2,3,2,1) # вернет 3
        most_common(1,2,3,2,1, 1,2,3,2,1) # вернет 3

"""
def most_common(my_list):
    count=0
    my_set=set()
    my_set=set(my_list)
    count=len(my_set)
    return count

print(set([1,2,3,2,3,6,5,4,4,4,4,4,]))
print(most_common([1,2,3,2,3,6,5,4,4,4,4,4,]))

"""
Нужно написать функцию которая сделать запрос по адресу http://jsonplaceholder.typicode.com/posts

        import requests
        response_list  = requests.get("http://jsonplaceholder.typicode.com/posts").json()
    
И нужно сделать пагинацию на список этих постов

Выводить нужно по 3 элемента и программа должна зависать , и после нажатия на Enter следующие 3 записи. (Используйте функцию input() )
"""

def request(my_list):
    for i in range(len(my_list)):
        print(my_list[i])
        if (i+1) % 3 == 0:
            x = input()


import requests
response_list = requests.get("http://jsonplaceholder.typicode.com/posts").json()

request(response_list)

"""
Сделать консольный обменник валют.
Дать пользователю возможность выбора валюты и типа операции (покупка/продажа) и показать ему правильный резульат (сколько он получит грн при продаже валюты , или сколько нужно грн чтобы купить валюту)

Курс валют брать с https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5
"""
import requests
response_list  = requests.get(" https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5").json()


print('''Currency exchange:
      1. USD<->UAH
      2. EUR<->UAH
      3. BTC<->USD
     ''')

choice = int(input('Choose currency: '))

if choice == 1:
    print('Your choice is USD<->UAH')
    print('''Available operations:
          1. Buy USD
          2. Sale USD
    ''')
    choice1=int(input('Choose desirable operation:'))
    if choice1==1:
        amount=float(input('How much USD you want to buy?'))
        print('You have to pay ', amount*float(response_list[0]["sale"]), 'of UAH')
    elif choice1==2:
        amount = float(input('How much USD you want to sale?'))
        print('You receive ', amount * float(response_list[0]["buy"]), 'of UAH')
    else:
        print('Wrong choice')

elif choice == 2:
    print('Your choice is EUR<->UAH')
    print('''Available operations:
              1. Buy EUR
              2. Sale EUR
        ''')
    choice1 = int(input('Choose desirable operation:'))
    if choice1 == 1:
        amount = float(input('How much EUR you want to buy?'))
        print('You have to pay ', amount * float(response_list[1]["sale"]), 'of UAH')
    elif choice1 == 2:
        amount = float(input('How much EUR you want to sale?'))
        print('You receive ', amount * float(response_list[1]["buy"]), 'of UAH')
    else:
        print('Wrong choice')

elif choice == 3:
    print('Your choice is BTC<->USD')
    print('''Available operations:
              1. Buy BTC
              2. Sale BTC
        ''')
    choice1 = int(input('Choose desirable operation:'))
    if choice1 == 1:
        amount = float(input('How much BTC you want to buy?'))
        print('You have to pay ', amount * float(response_list[2]["sale"]), 'of USD')
    elif choice1 == 2:
        amount = float(input('How much BTC you want to sale?'))
        print('You receive ', amount * float(response_list[2]["buy"]), 'of USD')
    else:
        print('Wrong choice')
else:
    print('Wrong choice')

"""
Напишите функцию, которая создаёт комбинацию двух списков таким образом: 
на вход два списка [1, 2, 3] (+) [11, 22, 33] -> [1, 11, 2, 22, 3, 33]
"""

def comb_list(list1,listt):
    list_result=[]
    list2=sorted(listt)
    k=0
    i=0
    if len(list1)<=len(list2):
        for j in range(len(list1)*2):
            if j % 2 == 0:
                list_result.append(list1[k])
                k+=1
            else:
                list_result.append(list2[i])
                i+=1
        return list_result

    else:
        for j in range(len(list2)*2):
            if j % 2 == 0:
                list_result.append(list1[k])
                k+=1
            else:
                list_result.append(list2[i])
                i+=1
    return list_result



print(comb_list([1, 2, 3], [33,11,22]))












