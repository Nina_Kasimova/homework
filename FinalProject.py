import json

acount=[{'login': 'Nina', 'password': 'qweqwe!adSD'},{'login': 'Olya', 'password': 'qwertyQWERTY!'}]

with open('acount', 'w') as file:
    json.dump(acount, file)
with open('acount', 'r') as file:
    user_acount = json.load(file)

#наприклад в базі є історія якогось користувача, де вказано розділ, категорію, кількість питань, на які він встиг відповісти, кількість балів і оцінка

history=[{'user':{'login':'Nina', 'chapter': 'Mathematical analysis', 'category': 'Functions on one variable', 'quntity_ques': 2, 'mark': 8}}]

with open('history', 'w') as file:
    json.dump(history, file)
with open('history', 'r') as file:
    us_history = json.load(file)

class Question:
    def __init__(self, question, answer, mark):
        self.question = question
        self.answer = answer
        self.mark = mark

    def get_mark(self):
        return self.mark

    def __eq__(self, other):
        return self.question==other.question and self.answer==other.answer and self.mark==other.mark


class Category:
    def __init__(self, name_category):
        self.name_category=name_category
        self.questions=[]


    def add_question(self, question):
        self.questions.append(question)

    def del_question(self,question):
        for elem in self.questions:
            if elem==question:
                self.questions.remove(question)


class Chapter:
    def __init__(self, name_chapter):
        self.name_chapter=name_chapter
        self.categories=[]

    def add_category(self, category):
        self.categories.append(category)

    def del_category(self,category):
        for elem in self.categories:
            if elem==category:
                self.categories.remove(category)



class Respondent:
    def __init__(self, name, surname, adress, phone_number, login, password):
        self.validate_user_data(password=password, phone_number=phone_number)
        self.phone_number=phone_number
        self.__password=password
        self.name = name
        self.surname=surname
        self. adress=adress
        self. login=login



    def __str__(self):
        return "{} {} {} {}".format(self.name, self.surname, self.adress, self.phone_number)

    def validate_password(self, password):
        count_upper_chars = 0
        count_spec_chars = 0
        spec_chars = '.,!?;'
        for elem in password:
             if ord(elem) in range(ord('A'), ord('Z') + 1):
                 count_upper_chars += 1
             if elem in spec_chars:
                 count_spec_chars += 1
        if count_upper_chars < 2 or count_spec_chars < 1:
            raise ValueError(' "{}"  invalid password!'.format(password))

    def validate_number(self, phone_number):
        if not (phone_number[1:].isdigit() and len(phone_number)==13 and phone_number[0:4]=='+380'):
            raise ValueError('{} invalid phone number'.format(phone_number))

    def validate_user_data(self, password, phone_number):
        self.validate_password(password)
        self.validate_number(phone_number)

    def write_to_json(self):
        data=[{'name': self.name, 'surname': self.surname, 'adress': self.adress, 'number': self.phone_number} ]
        with open('data', 'w') as file:
            json.dump(data, file)






#тут якраз рахуємо результат - суму балів за відповідну категорію, аргументом буде список набраних респондентом балів
    def result(self,questions_mark):
        sum=0
        for i in range(len(questions_mark)):
            sum+=questions_mark[i]
        return sum



#формуємо список-історію для респондента
    def user_history(self, login, chapter_name, category_name, number, number1):
        history.append({'user': {'login': login, 'chapter': chapter_name, 'category': category_name, 'quntity_ques': number, 'mark':number1}})
        return history









#дивимось перетин списків правильних відповідей згідно тесту і відповідей респондента - це буде кількість правильних відповідей респондента
    def right_answers(self,answer1,answer2):
        k=0
        for i in range(len(answer1)):
            for j in range(len(answer2)):
                if answer1[i]==answer2[j] and i==j:
                    k+=1
        return k



    # можна винести за класи
    def procent(self,b,a):
        return b / a * 100


    def list_answers(self,questions):
        l_a=[]
        for elem in questions:
            l_a.append(elem.answer)
        return l_a

#можна винести за класи

    def list_questions(self,questions):
        l_q=[]
        for elem in questions:
            l_q.append(elem)
        return l_q


    def enter_acount(self, login, password):
        if {'login': login, 'password': password} in user_acount:
            print('You are in acount')
        else:
            print('You are not registered user')


    def registration(self,login,password):
        self.login=login
        self.password=password
        user_acount.append({'login':login, 'password': password})
        #with open('acount', 'w') as file:
            #json.dump(user_acount, file)

    def get_password(self):
        return self.__password

    def exit_acount(self):
        print('Bye!')
        exit()


class Admin:
    def __init__(self, login, password):
        self.validate_password(password=password)
        self.__password = password
        self.login=login
        self.users=[]
        self.chapters=[]

    def add_chapter(self, chapter):
        self.chapters.append(chapter)

    def del_chapter(self,chapter):
        for elem in self.chapters:
            if elem==chapter:
                self.users.remove(chapter)

    def add_category1(self,chapter,category):
        for elem in self.chapters:
            if elem.name_chapter==chapter:
                elem.add_category(category)

    def del_category1(self,chapter, category):
        for elem in self.chapters:
            if elem.name_chapter==chapter:
                for elem1 in elem.categories:
                    if elem1==category:
                        elem.remove_category(category)


    def add_question1(self,question,category, chapter):
        for elem in self.chapters:
            if elem.name_chapter==chapter:
                for elem1 in elem.categories:
                    if elem1.name_category==category:
                        elem1.add_question(question)



    def del_question1(self,question,category, chapter):
        for elem in self.chapters:
            if elem.name_chapter==chapter:
                for elem1 in elem.categories:
                    if elem1.name_category==category:
                        elem1.del_question(question)


#скільки тестів пройшов юзер
    def statistics_test(self,history,user_name):
        k=0
        for elem in history:
            if elem['user']['login']==user_name:
                k+=1
        return k
#скільки категорій охоплено в тестах
    def statistics_cat(self,history):

        l=[]
        for elem in history:
            l.append(elem['user']['category'])

        return len(l)

# скільки тестів відповідної категорії пройдено
    def statistics_cat1(self, history, category_name):
        l = []
        for elem in history:
            if elem['user']['category'] == category_name:
                l.append(elem['user']['category'])
        return len(l)

#скільки розділів охоплено в пройдених тестах
    def statistics_chapter(self,history):
        l=[]
        for elem in history:
            l.append(elem['user']['chapter'])
        return len(l)

#скільки балів набрано за конкретний тест конкретного юзера
    def statistics_mark(self,user_name,category_name):
        for elem in history:
            if elem['user']['login']==user_name:
                if elem['user']['category']==category_name:
                    return elem['user']['mark']

#скільки тестів пройдено загалом
    def statistics_general(self, history):
        return len(history)


    def add_user(self, user):
        self.users.append(user)

    def del_user(self,user):
        for elem in self.users:
            if elem==user:
                self.users.remove(user)

    def change_userlogin(self,user, login):
        for elem in self.users:
            if elem==user:
                elem.login=login

    def change_usernumber(self,user, number):
        for elem in self.users:
            if elem==user:
                elem.phone_number=number

    def change_useradress(self,user, adress):
        for elem in self.users:
            if elem==user:
                elem.adress=adress

    def change_username(self,user, name):
        for elem in self.users:
            if elem==user:
                elem.name=name

    def change_usersurname(self,user, surname):
        for elem in self.users:
            if elem==user:
                elem.surname=surname

    def change_userpassword(self,user, password):
        for elem in self.users:
            if elem==user:
                elem.__password=password




    def validate_password(self, password):
        count_upper_chars = 0
        count_spec_chars = 0
        spec_chars = '.,!?;'
        for elem in password:
             if ord(elem) in range(ord('A'), ord('Z') + 1):
                 count_upper_chars += 1
             if elem in spec_chars:
                 count_spec_chars += 1
        if count_upper_chars < 2 or count_spec_chars < 1:
            raise ValueError(' "{}"  invalid password!'.format(password))

    def change_password(self, login, new_password):
         self.validate_password(new_password)
         if login == self.login:
             self.__password = new_password
             return True
         else:
             raise ValueError('login is incorrect')

    def change_login(self, new_login):
        self.login = new_login
        return True



    def enter_adminacount(self, login, password,list1):
        if {'login': login, 'password': password} in list1:
            print('You have rights of admin')
        else:
            print('You are not registered as admin')

    def get_password(self):
        return self.__password




print()
admin=[]
#реєструємо адміна
admin1=Admin('admin','qwerty!!OOOOOOO')

#зберігаємо його в систему
admin.append({'login':admin1.login, 'password':admin1.get_password()})
with open('admin', 'w') as file:
    json.dump(admin, file)
with open('admin', 'r') as file:
    admin_acount = json.load(file)

#здійснюємо вхід під адміном
admin1.enter_adminacount(admin1.login, admin1.get_password(), admin_acount)
print()
#створюємо юзерів
user1=Respondent('Vasya', 'Ivanenko', 'Naukova', '+380969887878', 'login', 'password!!!JJJJJ')
admin1.add_user(user1)
user2=Respondent('Olya','Petrenko', 'Franka', '+380976785645','login1','passHJJJKKK!!!!!!')
admin1.add_user(user2)
user3=Respondent('Oleg','Nazarenko', 'Chornovola', '+380976785646','login2','passHJJJKKK!!!!!!?')
admin1.add_user(user3)
for elem in admin1.users:
    print(elem)

print()
#спробуємо видалити юзера
admin1.del_user(user1)
for elem in admin1.users:
    print(elem)

print()
#спробуємо комусь змінити дані
admin1.change_useradress(user2,'Zdanovskoi')
for elem in admin1.users:
    print(elem)


#пробуємо створити тест

question1=Question('Is the equation y_der=f(x)g(y) the equation with separated variables?', 'Yes', 4)
question2=Question('Is the equation y_der =f(x,y) the equation of second order?', 'No', 4 )
question3=Question('Is the equation y_der=f(y/x) the homogeneous equation?', 'Yes', 4)
chapter1=Chapter('Mathematics')
category_math1=Category('Differential Equations')
category_math2=Category('Mathematical Analysis')

category_math1.add_question(question1)
category_math1.add_question(question2)
category_math1.add_question(question3)
print()

#надрукуємо, дивимось що вийшло
for elem in category_math1.questions:
    print(elem.question)


question11=Question('Does rational preference relation always have an utility function?', 'No', 5)
question22=Question('Is the lexicograthical preference relation transitive?', 'Yes', 5 )
question33=Question('Is the Cobb-Douglas production function unique for all production processes?', 'No', 5)
chapter2=Chapter('Mathematical Economics')
category_econ1=Category('Consumer Theory')
category_econ2=Category('Production Theory')
category_econ1.add_question(question11)
category_econ1.add_question(question22)
category_econ2.add_question(question33)




admin1.add_chapter(chapter1)
admin1.add_chapter(chapter2)
admin1.add_category1(chapter1.name_chapter, category_math1)
admin1.add_category1(chapter1.name_chapter,category_math2)
admin1.add_category1(chapter2.name_chapter,category_econ1)
admin1.add_category1(chapter2.name_chapter,category_econ2)
for elem in admin1.chapters:
    print(elem.name_chapter)

for elem in admin1.chapters:
    for elem1 in elem.categories:
        print(elem1.name_category)

for elem in admin1.chapters:
    for elem1 in elem.categories:
        for elem2 in elem1.questions:
            print(elem2.question)


print()
#Нехай хтось із респондентів хоче зайти в акаунт
user1.enter_acount(user1.login, user1.get_password())
print()
#він у нас не зареєстрований, тому реєструємось
user1.registration(user1.login, user1.get_password())
user1.enter_acount(user1.login, user1.get_password())

#будемо пробувати проходити якийсь тест
print('Let us begin testing')
print('There are such chapters')
for elem in admin1.chapters:
    print(elem.name_chapter)
chap=input('Choose chapter-->')
print('There are such categories of this chapter')
for elem in admin1.chapters:
    if elem.name_chapter==chap:
        for elem1 in elem.categories:
            print(elem1.name_category)
cat=input('Choose category-->')
listt=[]
for elem in admin1.chapters:
    if elem.name_chapter==chap:
        for elem1 in elem.categories:
            if elem1.name_category==cat:
                for elem2 in elem1.questions:
                    listt.append(elem2)


li=[] #будемо формувати список відповідей респондента
li1=[] #будемо формувати список балів за відповіді
for elem in listt:
    print(elem.question)
    ans1=input('Your answer-->')
    li.append(ans1)
    if ans1==elem.answer:
        li1.append(elem.mark)
    else:
        li1.append(0)
    agree=input('Do You want to continue?(Y/N)-->')
    if agree=='N':
        break

#список відповідей респондента
print('Your answers')
print(li)
l=len(li)
#список набраних балів
print('Your marks')
print(li1)
#формуємо список відповідей з тесту, обрубуємо його на довжині, що дорівнює кількості питань, що пройшов респондент, він же має право перервати процес
print('Right answers')
print(user1.list_answers(listt)[0:len(li)])
#дивимось скільки правильних відповідей
print('Your right answers')

print(user1.right_answers(user1.list_answers(listt)[0:len(li)],li))



#дивимось скільки в сумі балів
print('Result=sum of marks')
kv=user1.result(li1)
print(kv)



#Додаємо респондента в загальну історію
us_history=user1.user_history(user1.login, chap, cat, l, kv)
print('All history')
print(us_history)

#виводимо по логіну тільки історію нашого респондента
print('Your history')
for elem in us_history:
    if elem['user']['login']==user1.login:
        print(elem)


#спробуємо подивитись якусь статитстику
#скільки тестів пройшов конкретний юзер, з яким зараз працюємо
print(admin1.statistics_test(us_history, user1.login))

#скільки категорій охоплено в тестах
print(admin1.statistics_cat(us_history))

#скільки тестів відповідної категорії пройдено
print(admin1.statistics_cat1(us_history,cat))
#скільки балів набрано за конкретний тест конкретного юзера
print(admin1.statistics_mark(user1.login, cat))
#скільки тестів пройдено загалом
print(admin1.statistics_general(us_history))
#скільки розділів охоплено в пройдених тестах
print(admin1.statistics_chapter(us_history))
